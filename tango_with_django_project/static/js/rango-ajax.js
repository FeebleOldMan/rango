$(document).ready(function() {

    function getCookie(name) {
let cookieValue = null;
if (document.cookie && document.cookie !== '') {
    const cookies = document.cookie.split(';');
for (let i = 0; i < cookies.length; i++) {
let cookie = jQuery.trim(cookies[i]);
if (cookie.substring(0, name.length + 1) === (name + '=')) {
cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
break;
}
}
}
return cookieValue;
}

    $("#likes").click(function() {
        const catid = $(this).attr("data-catid");
        $.get(
            like_url, 
            {category_id: catid},
            function(data) {
                $("#like_count").html(data);
                $("#likes").hide();
            }
        );
    });

    $("#suggestion").keyup(function() {
        const query = $(this).val();
        $.get(
            suggest_url,
            {suggestion: query},
            function(data) {
                $("#cats").html(data);
            }
        );
    });

$(".rango-add").click(function(){
    const catid = $(this).attr("data-catid");
    const title = $(this).attr("data-title");
    const url = $(this).attr("data-url");
    const csrf = getCookie('csrftoken');
    const me = $(this)
    $.post(
        auto_add_url,
        {catid: catid, title: title, url: url, csrfmiddlewaretoken: csrf},
        function(data) {
$("#page").html(data);
        me.hide();
        }
    );
        
});

});
