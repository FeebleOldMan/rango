import json
import urllib.parse
import urllib.request
import http.client
#import sys

def read_bing_key():
    """
    Reads the Bing API key from a file called 'bing.key'.
    returns: a string which is either None, i.e. no key found, or with a key
    Remember: put bing.key in your .gitignore file to avoid committing it!
    """
    # See Python Anti-Patterns - it's an awesome resource!
    # Here we are using "with" when opening documents.
    # http://docs.quantifiedcode.com/python-anti-patterns/maintainability/
    bing_api_key = None

    try:
        with open('bing.key', 'r') as f:
            bing_api_key = f.readline()
    except:
        raise IOError('bing.key file not found')

    return bing_api_key

def run_query(search_terms):
    bing_api_key = read_bing_key().strip('\n')
    if not bing_api_key:
        raise KeyError("Bing Key Not Found")
    headers = {
        'Ocp-Apim-Subscription-Key': bing_api_key,
    }

    results_per_page = 10
    offset = 0

    params = urllib.parse.urlencode({
        'q': search_terms,
        'count': results_per_page,
        'offset': offset,
        'safesearch': 'Moderate',
    })

    results = []

    try:
        conn = http.client.HTTPSConnection('api.cognitive.microsoft.com')
#        with conn.request("GET", "/bing/v5.0/search?%s" % params, "{body}", headers) as f:
#            response = f.getresponse()
#            data = response.read()
        conn.request("GET", "/bing/v5.0/search?%s" % params, "{body}", headers)
        response = conn.getresponse()
        data = response.read()
        data = data.decode('utf-8')
        json_response = json.loads(data)
        for result in json_response['webPages']['value']:
            results.append({'title': result['name'],
                            'link': result['url'],
                            'summary': result['snippet']})
    except Exception as e:
        #print("[Errno {0}] {1}".format(e.errno, e.strerror))
        print("Error: {}".format(e))

    return results

### tango with django version outdated and doens't work
#def run_query(search_terms):
#    """
#    Given a string containing search terms (query),
#    returns a list of results from the Bing search engine.
#    """
#    bing_api_key = read_bing_key()
##    bing_api_key = 'Basic ' + (':%s' % bing_api_key).encode('base64')[:-1]
#
#    if not bing_api_key:
#        raise KeyError("Bing Key Not Found")
#
#    # Specify the base url and the service (Bing Search API 2.0)
#    #root_url = 'https://api.datamarket.azure.com/Bing/Search/v1/'
#    #service = 'Web'
#
#    # updated to v5
#    root_url =  'https://api.cognitive.microsoft.com/bing/v5.0/'
#    service = 'search'
#
#    # Specify how many results we wish to be returned per page.
#    # Offset specifies where in the results list to start from.
#    # With results_per_page = 10 and offset = 11, this would start from page 2.
#    results_per_page = 10
#    offset = 0
#
#    # Wrap quotes around our query terms as required by the Bing API.
#    # The query we will then use is stored within variable query.
#    #query = "'{0}'".format(search_terms)
#    query = search_terms
#
#    # Turn the query into an HTML encoded string, using urllib.
#    query = urllib.parse.quote(query)
#
#    # Construct the latter part of our request's URL.
#    # Sets the format of the response to JSON and sets other properties.
#    #search_url = "{0}{1}?$format=json&$top={2}&$skip={3}&Query={4}".format(
#    #                root_url,
#    #                service,
#    #                results_per_page,
#    #                offset,
#    #                query)
#    search_url = "{0}{1}?q={2}&count={3}&offset={4}&safesearch=Moderate".format(
#        root_url, service, query, results_per_page, offset)
#
#    # debug
#    print(search_url)
#
#    # Setup authentication with the Bing servers.
#    # The username MUST be a blank string, and put in your API key!
#    username = ''
#
#    # Setup a password manager to help authenticate our request.
#    password_mgr = urllib.request.HTTPPasswordMgrWithDefaultRealm()
#    password_mgr.add_password(None, search_url, username, bing_api_key)
#
#    # Create our results list which we'll populate.
#    results = []
#
#    try:
#        # Prepare for connecting to Bing's servers
#        try:
#            handler = urllib.request.HTTPBasicAuthHandler(password_mgr)
##            handler = urllib.request.HTTPBasicAuthHandler()
##            handler.add_password(None, search_url, username, bing_api_key)
#        except ValueError as err:
#            print('HTTPBasicAuthHandler Error:', err)
#        opener = urllib.request.build_opener(handler)
#        urllib.request.install_opener(opener)
#
#        # Connect to the server and read the response generated
#        #response = urllib.request.urlopen(search_url).read()
#        #response = response.decode('utf-8')
#
#        try:
#            with urllib.request.urlopen(search_url) as f:
#                response = f.read().decode('utf-8')
#        except urllib.error.HTTPError as err:
#            print('urlopen HTTPError:', err)
##        except error.URLError as err:
##            print('urlopen URLError:', err)
##        except:
##            err = sys.exc_info()[0]
##            print('urlopen General Error:', err)
#
#        # Convert the string response to a Python dictionary object
#        #json_response = json.loads(response)
#        json_response = json.loads(response.text)
#
#        # Loop through each page returned, populating out results list.
#        for result in json_response['d']['results']:
#            results.append({'title': result['Title'],
#                            'link': result['Url'],
#                            'summary': result['Description']})
#    except:
#        print("Error when querying the Bing API")
#
#        json_response = json.loads(
#                '{"d":{"results":[{"__metadata":{"uri":"https://api.datamarket.azure.com/Data.ashx/Bing/Search/v1/Web?Query=\u0027google\u0027&$skip=0&$top=1","type":"WebResult"},"ID":"908f90c5-f2a3-45db-b8bf-e4acd7a32f7e","Title":"Google","Description":"Google.com.sg offered in: 中文(简体) Bahasa Melayu ...","DisplayUrl":"www.google.com.sg","Url":"http://www.google.com.sg/"},{"__metadata":{"uri":"https://api.datamarket.azure.com/Data.ashx/Bing/Search/v1/Web?Query=\u0027google\u0027&$skip=1&$top=1","type":"WebResult"},"ID":"9f997753-c1e0-41f0-b39a-88ceecab0b1d","Title":"Google","Description":"Search the world\u0027s information, including webpages, images, videos and more. Google has many special features to help you find exactly what you\u0027re looking for.","DisplayUrl":"www.google.com","Url":"http://www.google.com/"},{"__metadata":{"uri":"https://api.datamarket.azure.com/Data.ashx/Bing/Search/v1/Web?Query=\u0027google\u0027&$skip=2&$top=1","type":"WebResult"},"ID":"b46f4ba8-86ad-419e-8540-6d41f8ff05a2","Title":"Google","Description":"Google.com.my offered in: Bahasa Malaysia","DisplayUrl":"www.google.com.my","Url":"http://www.google.com.my/"},{"__metadata":{"uri":"https://api.datamarket.azure.com/Data.ashx/Bing/Search/v1/Web?Query=\u0027google\u0027&$skip=3&$top=1","type":"WebResult"},"ID":"dbc5e845-6aa9-4fd9-a881-91f9cdf63684","Title":"Google India","Description":"Indian version of the search engine. Search the web or only webpages from India. Offered in English, Hindi, Bengali, Telugu, Marathi, Tamil, Gujarati, Kannada, ...","DisplayUrl":"www.google.co.in","Url":"http://www.google.co.in/"},{"__metadata":{"uri":"https://api.datamarket.azure.com/Data.ashx/Bing/Search/v1/Web?Query=\u0027google\u0027&$skip=4&$top=1","type":"WebResult"},"ID":"6203c237-4ff0-476f-9854-dfac662870e1","Title":"Google","Description":"Google.com.ph offered in: Filipino Cebuano. Advertising Programs Business Solutions About Google Google.com © 2016 - Privacy - Terms. Search; Images; Maps; Play ...","DisplayUrl":"www.google.com.ph","Url":"http://www.google.com.ph/"},{"__metadata":{"uri":"https://api.datamarket.azure.com/Data.ashx/Bing/Search/v1/Web?Query=\u0027google\u0027&$skip=5&$top=1","type":"WebResult"},"ID":"541d61ea-ffe3-4293-83de-5a0a1bb4cb5d","Title":"Google","Description":"South Africa : Advanced search Language tools: Google.co.za offered in: Afrikaans Sesotho isiZulu IsiXhosa Setswana Northern Sotho","DisplayUrl":"www.google.co.za","Url":"http://www.google.co.za/"},{"__metadata":{"uri":"https://api.datamarket.azure.com/Data.ashx/Bing/Search/v1/Web?Query=\u0027google\u0027&$skip=6&$top=1","type":"WebResult"},"ID":"91b2a6ed-0140-4fd1-b65e-e8f69c887a6f","Title":"Google","Description":"Offers the choice of searching the whole web or web pages from Australia. Also advanced search, image and groups search, news and directory from the Open Directory.","DisplayUrl":"www.google.com.au","Url":"http://www.google.com.au/"},{"__metadata":{"uri":"https://api.datamarket.azure.com/Data.ashx/Bing/Search/v1/Web?Query=\u0027google\u0027&$skip=7&$top=1","type":"WebResult"},"ID":"2915830c-74c1-4e2e-8d27-8b33e020cb3c","Title":"Google","Description":"Cybersikkerhedsmåned. Tjek på 2 min., at du er sikker online. Google.dk på: Føroyskt. Annoncér med Google Virksomhedsløsninger +Google Alt om Google Google.com","DisplayUrl":"www.google.dk","Url":"http://www.google.dk/"},{"__metadata":{"uri":"https://api.datamarket.azure.com/Data.ashx/Bing/Search/v1/Web?Query=\u0027google\u0027&$skip=8&$top=1","type":"WebResult"},"ID":"dc6bf3a5-de3c-4ad4-8eb9-47eb9d51d5c2","Title":"Google","Description":"Google.ca offered in: Français. Advertising Programs Business Solutions +Google About Google Google.com © 2016 - Privacy - Terms. Search; Images; Maps; Play ...","DisplayUrl":"www.google.ca","Url":"http://www.google.ca/"},{"__metadata":{"uri":"https://api.datamarket.azure.com/Data.ashx/Bing/Search/v1/Web?Query=\u0027google\u0027&$skip=9&$top=1","type":"WebResult"},"ID":"36f23819-be70-4364-b483-6c13572ea7ab","Title":"Google","Description":"Google.com.vn hiện đã có bằng các ngôn ngữ: English Français 中文（繁體）","DisplayUrl":"www.google.com.vn","Url":"http://www.google.com.vn/"}],"__next":"https://api.datamarket.azure.com/Data.ashx/Bing/Search/v1/Web?Query=\u0027google\u0027&$skip=10&$top=10"}}')
#        for result in json_response['d']['results']:
#            results.append({'title': result['Title'],
#                            'link': result['Url'],
#                            'summary': result['Description']})
#
#    # Return the list of results to the calling function
#    return results

if __name__ == '__main__':
    print(run_query(input("Type search term(s): ")))

