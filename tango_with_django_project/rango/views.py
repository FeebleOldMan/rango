from datetime import datetime
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.contrib.auth import authenticate
from django.utils import timezone
from rango.models import Category, Page, User, UserProfile
from rango.forms import CategoryForm, PageForm, UserProfileForm, UserUpdateForm
from rango.bing_search import run_query
from rango.forms import UserForm, UserProfileForm
#from django.contrib.auth import authenticate, login, logout

def index(request):
    # Cookie test
    #request.session.set_test_cookie()

    # Query the database for a list of ALL categories currently stored.
    # Order the categories by no. likes in descending order.
    # Retrieve the top 5 only - or all if less than 5.
    # Place the list in our context_dict dictionary
    # that will be passed to the template engine.

    category_list = Category.objects.order_by('-likes')[:5]
    page_list = Page.objects.order_by('-views')[:5]
    context_dict = {'categories': category_list, 'pages': page_list}

    ### switching to server side sessions
    # obtain our Response object early so we can add cookie information
    #response = render(request, 'rango/index.html', context_dict)

    # call function to handle the cookies
    # function modifies response cookie settings
    #visitor_cookie_handler(request, response)

    ### server side session cookie handling
    visitor_cookie_handler(request)
    context_dict['visits'] = request.session['visits']
    response = render(request, 'rango/index.html', context_dict)

    # return response back to the user, updating any cookies that need changed
    return response

    # pre-cookie implementation
    # Render the response and send it back!
    #return render(request, 'rango/index.html', context_dict)

    ### old test version
    ## construct a dictionary to pass to the template engine as its context
    ## key "boldmessage" is same as {{ boldmessage }} in template
    #context_dict = {'boldmessage': "Crunchy, creamy, cookie, candy, cupcake!"}

    ## return rendered response to send to client
    ## render shortcut function to make response construction easy
    ## first param is template we wish to use
    #return render(request, 'rango/index.html', context=context_dict)

# server side cookie helper method
def get_server_side_cookie(request, cookie, default_val=None):
    val = request.session.get(cookie)
    if not val:
        val = default_val
    return val

def visitor_cookie_handler(request):
    visits = int(get_server_side_cookie(request, 'visits', '1'))
    last_visit_cookie = get_server_side_cookie(request,
                                               'last_visit',
                                               str(datetime.now()))
    last_visit_time = datetime.strptime(last_visit_cookie[:-7],
                                        '%Y-%m-%d %H:%M:%S')

    # if it's been more than a day since the last visit...
    if (datetime.now() - last_visit_time).days > 0:
        visits += 1
        # update the last visit cookie now that we have updated the count
        request.session['last_visit'] = str(datetime.now())
    else:
        # set the last visit cookie
        request.session['last_visit'] = last_visit_cookie

    # update/set the visits cookie
    request.session['visits'] = visits

### visitor cookie helper function
### pre server-side version
#def visitor_cookie_handler(request, response):
#    # get the number of visits to the site.
#    # we use the COOKIES.get() function to obtain the visits cookie.
#    # if the cookie exists, the value returned is casted to an integer
#    # if the cookie doesn't exist, then the default value of 1 is used
#    visits = int(request.COOKIES.get('visits', '1'))
#
#    last_visit_cookie = request.COOKIES.get('last_visit', str(datetime.now()))
#    last_visit_time = datetime.strptime(last_visit_cookie[:-7],
#                                        '%Y-%m-%d %H:%M:%S')
#
#    # if it's been more than a day since the last visit...
#    if (datetime.now() - last_visit_time).days > 0:
#        visits += 1
#        # update the last visit cookie now that we've updated the count
#        response.set_cookie('last_visit', str(datetime.now()))
#    else:
#        # set the last visit cookie; stays the same since same day
#        response.set_cookie('last_visit', last_visit_cookie)
#
#    # update/set the visits cookie
#    response.set_cookie('visits', visits)

def about(request):
    # test cookie
    #if request.session.test_cookie_worked():
    #    print("TEST COOKIE WORKED!")
    #    request.session.delete_test_cookie()
    #else:
    #    print("NO COOKIES :(")

    return render(request, 'rango/about.html', {'visits': request.session.get('visits', None)})

def show_category(request, category_name_slug):
    # Create a context dictionary which we can pass
    # to the template rendering engine
    context_dict = {}

    try:
        # Can we find a category name slug with the given name?
        # If we can't, the .get() method raises a DoesNotExist exception.
        # So the .get() method returns one model instance or raises an exception.
        category = Category.objects.get(slug=category_name_slug)

        # Retrieve all of the associated pages.
        # Note that filter() will return a list of page objects or an empty list
        pages = Page.objects.filter(category=category).order_by('-views')

        # Adds our results list to the template context under name pages.
        context_dict['pages'] = pages
        # We also add the category object from
        # the database to the context dictionary.
        # We'll use this in the template to verify that the category exists.
        context_dict['category'] = category

    except Category.DoesNotExist:
        # We get here if we didn't find the specified category.
        # Don't do anything -
        # the template wil display the "no category" message for us.
        context_dict['category'] = None
        context_dict['pages'] = None

    ### Search function
    context_dict['query'] = category.name
    result_list = []
    query = ''
    if request.method == 'POST':
        query = request.POST['query'].strip()
        if query:
            # Run our Bing function to get the results list!
            result_list = run_query(query)
            context_dict['result_list'] = result_list
            context_dict['query'] = query

    # Go render the response and return it to the client.
    return render(request, 'rango/category.html', context_dict)

@login_required
def add_category(request):
    form = CategoryForm()

    # A HTTP POST?
    if request.method == 'POST':
        form = CategoryForm(request.POST)

        # Have we been provided with a valid form?
        if form.is_valid():
            # Save the new category to the database.
            #form.save(commit=True)
            cat = form.save(commit=True)
            # prints category to server console
            print(cat, cat.slug)
            # Now that the category is saved
            # We could give a confirmation message
            # But since the most recent category added is on the index page
            # Then we can just direct the user back to the index page.
            #return index(request)
            return redirect('rango:index')
        else:
            # The supplied form contained errors -
            # Just print them to the terminal
            print(form.errors)

    # Will handle the bad form, new form, or no form supplied cases.
    # Render the form with error messages (if any).
    return render(request, 'rango/add_category.html', {'form': form})

@login_required
def add_page(request, category_name_slug):
    try:
        category = Category.objects.get(slug=category_name_slug)
    except Category.DoesNotExist:
        category = None

    form = PageForm()

    if request.method == 'POST':
        form = PageForm(request.POST)

        if form.is_valid():
            if category:
                page = form.save(commit=False)
                page.category = category
                page.views = 0
                page.save()
                print("Page created:", page)
                return redirect('rango:show_category', category_name_slug)
        else:
            print(form.errors)

    context_dict = {'form': form, 'category': category}
    return render(request, 'rango/add_page.html', context_dict)

### UserForm, UserProfileForm
#def register(request):
#    # A boolean value for telling the template
#    # whether the registration was successful.
#    # Set to False initially. Code changes value to
#    # True when registration succeeds.
#    registered = False
#
#    # If it's a HTTP POST, we're interested in processing form data
#    if request.method == 'POST':
#        # Attempt to grab information from the raw form information.
#        # Note that we make use of both UserForm and UserProfileForm.
#        user_form = UserForm(data=request.POST)
#        profile_form = UserProfileForm(data=request.POST)
#
#        # If the two forms are valid...
#        if user_form.is_valid() and profile_form.is_valid():
#            # Save the user's form data to the database.
#            user = user_form.save()
#
#            # Now we hash the password with the set_password method.
#            # Once hashed, we can update the user object.
#            user.set_password(user.password)
#            user.save()
#
#            # Now sort out the UserProfile instance.
#            # Since we need to set the user attribute ourselves,
#            # we set commit=False. This delays saving the model
#            # until we're ready to avoid integrity problems.
#            profile = profile_form.save(commit=False)
#            profile.user = user
#
#            # Did the user provide a profile picture?
#            # If so, we need to get it from the input form and
#            # put it in the UserProfile model.
#            if 'picture' in request.FILES:
#                profile.picture = request.FILES['picture']
#
#            # Now we save the UserProfile model instance.
#            profile.save()
#
#            # Update our variable to indicate that the template
#            # registration was successful.
#            registered = True
#        else:
#            # Invalid form or forms - mistakes or something else?
#            # Print problems to the terminal.
#            print(user_form.errors, profile_form.errors)
#    else:
#        # Not a HTTP POST, so we render our form using two ModelForm instances.
#        # These forms will be blank, ready for user input.
#        user_form = UserForm()
#        profile_form = UserProfileForm()
#
#    # Render the template depending on the context.
#    return render(request, 'rango/register.html',
#                  {'user_form': user_form,
#                   'profile_form': profile_form,
#                   'registered': registered})
#
#def user_login(request):
#    context = {}
#    # If the request is a HTTP POST, try to pull out the relevant information.
#    if request.method == 'POST':
#        # Gather the username and password provided by the user.
#        # This information is obtained from the login form.
#        # We use request.POST.get('<variable>') as opposed
#        # to request.POST['<variable>'], because the
#        # request.POST.get('<variable>') returns None if the 
#        # value does not exist, while the request.POST['<variable>']
#        # will raise a KeyError exception.
#        # this is getting info from dictionary
#        username = request.POST.get('username')
#        password = request.POST.get('password')
#
#        # Use Django's machinery to attempt to see if the username/password
#        # combination is valid - a User object is returned if it is.
#        user = authenticate(username=username, password=password)
#
#        # If we have a User object, the details are correct.
#        # If None (Python's way of representing the absence of a value), no user
#        # with matching credentials was found.
#        if user:
#            # Is the account active? It could have been disabled.
#            if user.is_active:
#                # If the account is valid and active, we can log the user in.
#                # We'll send the user back to the homepage.
#                login(request, user)
#                return redirect('rango:index')
#            else:
#                # An inactive account was used - no loggin in!
#                context['error'] = "Your Rango account is disabled."
#        else:
#            # Bad login details were provided. So we can't log the user in.
#            print("Invalid login details: {0}, {1}".format(username, password))
#            context['username'] = username
#            context['error'] = "Invalid login details supplied."
#
#    # The request is not a HTTP POST, so display the login form.
#    # This scenario would most likely be a HTTP GET.
#    #else:
#        # No context variables to pass to the template system, hence the
#        # blank dictionary object...
#    return render(request, 'rango/login.html', context)

@login_required
def register_profile(request):
    if request.method == 'POST':
        profile_form = UserProfileForm(request.POST, request.FILES)
        if profile_form.is_valid():
            user_profile = profile_form.save(commit=False)
            user_profile.user = request.user
            user_profile.save()
            # success message
            messages.success(request, "User profile succesfully created!")
            return redirect('rango:index')
        else:
            print(form.errors)
    else:
        profile_form = UserProfileForm()
    return render(request, 'rango/profile_registration.html',
                  {'profile_form': profile_form})

@login_required
def profile(request):
    user_profile = UserProfile.objects.get(user=request.user)
    if request.method == 'POST':
        user_form = UserUpdateForm(request.POST, instance=request.user)
        profile_form = UserProfileForm(request.POST, instance=user_profile)
        if user_form.is_valid() and profile_form.is_valid():
            password = request.POST.get('password')
            if request.user.check_password(password):
                user_form.save()
                profile = profile_form.save(commit=False)
                if 'picture' in request.FILES:
                    profile.picture = request.FILES['picture']
                profile.save()
                messages.success(request, "Profile updated")
            else:
                messages.warning(request, "Invalid password")
        else:
            messages.warning(request, "Invalid form data")
    context = {'username': request.user.username, 'email': request.user.email, 'picture': user_profile.picture, 'website': user_profile.website}
    return render(request, 'rango/profile.html', context)

@login_required
def view_profiles(request):
    user_profiles = UserProfile.objects.all()
    return render(request, 'rango/view_profiles.html', {'user_profiles': user_profiles})

@login_required
def profile_detail(request, profile_name):
    user = User.objects.get(username=profile_name)
    user_profile = UserProfile.objects.get(user=user)
    context = {'username': user.username, 'email': user.email, 'picture': user_profile.picture, 'website': user_profile.website}
    return render(request, 'rango/profile_detail.html', context)

@login_required
def restricted(request):
    return render(request, 'rango/restricted.html', {})

# Use the login_required decorator to ensure only logged in users can get view
#@login_required
#def user_logout(request):
#    # Since we know the user is logged in, we can now just log them out
#    logout(request)
#    # Take user back to homepage
#    return redirect('rango:index')

def search(request):
    result_list = []
    if request.method == 'POST':
        query = request.POST['query'].strip()
        if query:
            # Run our Bing function to get the results list!
            result_list = run_query(query)
    else:
        query = ''
    return render(request, 'rango/search.html', {'result_list': result_list, 'query': query})

def track_url(request):
    page_id = request.GET.get('page_id')
    if page_id:
        page = Page.objects.get(pk=int(page_id))
        page.views += 1
        if not page.first_visit:
            page.first_visit = timezone.now()
        page.last_visit = timezone.now()
        url = page.url
        page = page.save()
        return redirect(url)
    else:
        return redirect('rango:index')

@login_required
def like_category(request):
    category_id = request.GET.get('category_id')
    if category_id:
        category = Category.objects.get(id=int(category_id))
        if category:
            category.likes += 1
            category.save()
            return HttpResponse(category.likes)
    return redirect('rango:index')

def get_category_list(max_results=0, starts_with=''):
    if max_results == 0:
        categories = Category.objects.all()
    else:
        categories = Category.objects.filter(name__istartswith=starts_with)[:max_results]
    return categories

def suggest_category(request):
    query = request.GET.get('suggestion')
    if query:
        categories = get_category_list(8, query)
    else:
        categories = get_category_list()
    return render(request, 'rango/cats.html', {'cats': categories})

@login_required
def auto_add_page(request):
    if request.method == 'POST':
        title = request.POST.get('title')
        url = request.POST.get('url')
        catid = request.POST.get('catid')
        print(title, url, catid)
        category = Category.objects.get(pk=int(catid))
        if category:
            page = Page.objects.get_or_create(category=category, title=title, url=url)
    pages = Page.objects.filter(category=category).order_by('-views')
    return render(request, 'rango/pages.html', {'pages': pages})

