from django.db import models
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify
from django.utils import timezone

### MODEL ORDER 
# constants
# fields
# custom manager
# meta
# def __str__
# other special methods
# def clean
# def save
# def get_absolute_url
# other methods

class UserProfile(models.Model):
    # This line is required. Links UserProfile to a User model instance.
    user = models.OneToOneField(User)

    # The additional attributes we wish to include.
    website = models.URLField(blank = True)
    picture = models.ImageField(upload_to='profile_images', blank=True)

    # Override the __unicode() method to return out something meaningful!
    def __str__(self):
        return self.user.username

class Category(models.Model):
    LEN_NAME = 128
    name = models.CharField(max_length=LEN_NAME, unique=True)
    views = models.PositiveIntegerField(default=0)
    likes = models.PositiveIntegerField(default=0)
    slug = models.SlugField(unique=True)

    class Meta:
        verbose_name_plural = 'categories'

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        if self.views < 0: self.views = 0
        if self.likes < 0: self.likes = 0
        super(Category, self).save(*args, **kwargs)

class Page(models.Model):
    LEN_TITLE = 128
    category = models.ForeignKey(Category)
    title = models.CharField(max_length=LEN_TITLE)
    url = models.URLField()
    views = models.IntegerField(default=0)
    last_visit = models.DateTimeField(null=True)
    first_visit = models.DateTimeField(null=True)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        now = timezone.now()
        if self.last_visit and self.last_visit > now:
            self.last_visit = now
        if self.first_visit and self.first_visit > now:
            self.first_visit = now
        super(Page, self).save(*args, **kwargs)
