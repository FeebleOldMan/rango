import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE',
                       'tango_with_django_project.settings')

import django
django.setup()
from rango.models import Category, Page

def populate():
    # First, we will create lists of dictionaries containing the pages
    # we want to add into each category.
    # Then we will create a dictionary of dictionaries for our categories.
    # This might seem a little bit confusing, but it allows us to iterate
    # through each data structure, and add the data to our models.

    python_pages = [
        {"title": "Official Python Tutorial",
         "url": "http://docs.python.org/3/tutorial/",
         "views": 128},
        {"title": "How to Think like a Computer Scientist",
         "url": "http://www.greenteapress.com/thinkpython/",
         "views": 64},
        {"title": "Learn Python in 10 Minutes",
         "url": "http://www.korokithakis.net/tutorials/python/",
         "views": 32} ]

    django_pages = [
        {"title": "Official Django Tutorial",
         "url": "https://docs.djangoproject.com/en/1.10/intro/tutorial01/",
         "views": 256},
        {"title": "Django Rocks",
         "url": "http://www.djangorocks.com/",
         "views": 512},
        {"title": "How to Tango with Django",
         "url": "http://www.tangowithdjango.com/",
         "views": 1024} ]

    other_pages = [
        {"title": "Bottle",
         "url": "http://bottlepy.org/docs/dev/",
         "views": 4},
        {"title": "Flask",
         "url": "http://flask.pocoo.org",
         "views": 8} ]

    pug_pages = []

    reddit_pages = [
        {"title": "EDC",
         "url": "https://www.reddit.com/r/edc",
         "views": 64},
        {"title": "aww",
         "url": "https://www.reddit.com/r/aww",
         "views": 128}
        ]

    pascal_pages = []
    perl_pages = []
    php_pages = []
    prolog_pages = []
    postscript_pages = []
    programming_pages = []

    cats = {
        "Python": {
            "pages": python_pages,
            "views": 128,
            "likes": 64},
        "Django": {
            "pages": django_pages,
            "views": 64,
            "likes": 32},
        "Other Frameworks": {
            "pages": other_pages,
            "views": 32,
            "likes": 16},
        "Python User Groups": {
            "pages": pug_pages,
            "views": 16,
            "likes": 8},
        "Reddit": {
            "pages": reddit_pages,
            "views": 256,
            "likes": 1024},
        "Pascal": {
            "pages": pascal_pages,
            "views": 4,
            "likes": 0},
        "Perl": {
            "pages": perl_pages,
            "views": 2,
            "likes": 1},
        "PHP": {
            "pages": php_pages,
            "views": 8,
            "likes": 0},
        "Prolog": {
            "pages": prolog_pages,
            "views": 0,
            "likes": 0},
        "PostScript": {
            "pages": postscript_pages,
            "views": 1,
            "likes": 0},
        "Programming": {
            "pages": programming_pages,
            "views": 32,
            "likes": 16}
        }

    # If you want to add more categories or pages,
    # add them to the dicitonaries above

    # The code below goes through the cats dictionary, then adds each category,
    # and then adds all the associated pages for that category.

    for cat, cat_data in cats.items():
        print(cat)
        c = add_cat(cat, cat_data["views"], cat_data["likes"])
        for p in cat_data["pages"]:
            add_page(c, p["title"], p["url"], p["views"])

    # Print out the categories we have added.

    for c in Category.objects.all():
        for p in Page.objects.filter(category=c):
            print("- {0} - {1}".format(str(c), str(p)))

def add_page(cat, title, url, views=0):
    p = Page.objects.get_or_create(category=cat, title=title)[0]
    p.url = url
    p.views = views
    p.save()
    return p

def add_cat(name, views=0, likes=0):
    c = Category.objects.get_or_create(name=name, views=views, likes=likes)[0]
    c.save()
    return c

# Start execution here!
# code below only runs if executed as a standalone. if imported, will not run
if __name__ == '__main__':
    print("Starting Rango population script...")
    populate()

